
echo "Descargando tema:"

if [ ! -d "public/startbootstrap-coming-soon" ]; then

    if [ ! -d "vendor" ]; then
        echo "Creando directorio 'vendor'"
        mkdir vendor
    fi
    cd vendor

    if [ ! -d "startbootstrap-coming-soon" ]; then
        git clone https://github.com/BlackrockDigital/startbootstrap-coming-soon.git
    fi

    cp startbootstrap-coming-soon ../public/startbootstrap-coming-soon -r
    cd ..

fi
echo "Hecho."
