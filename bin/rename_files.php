#!/usr/bin/env php
<?php

$location = '/home/diego/coding/archivo';

rename_files($location);

function rename_files($location, $exec=false) {

    $s = DIRECTORY_SEPARATOR;

    echo "Se renombran los archivos contenidos en: $location\n\n";
    $years = scandir($location);
    echo "paso years\n\n";

    foreach ($years as $year) {
        if ($year == '.') continue;
        if ($year == '..') continue;
        // die;

        $months = scandir("$location{$s}$year");

        foreach ($months as $month) {
            if ($month == '.') continue;
            if ($month == '..') continue;

            $files = scandir("$location{$s}$year{$s}$month");
            $n = 1;
            $sfiles = [];
            $path = "$location{$s}$year{$s}$month";

            foreach (sort_files($files, $path) as $file) {
                if ($file == '.') continue;
                if ($file == '..') continue;


                echo "Origen:  $path{$s}$file\n";

                $dest = $n < 10? "$path{$s}$year-$month-00$n" : ($n < 100? "$path{$s}$year-$month-0$n" : "$path{$s}$year-$month-$n");
                echo "Destino: $dest\n\n";
                if ($exec) rename("$path{$s}$file", $dest);
                $n++;
            }
        }
    }
}

function sort_files($files, $path) {
    $s = DIRECTORY_SEPARATOR;
    $sfiles = [];

    foreach ($files as $file) {
        if ($file == '.') continue;
        if ($file == '..') continue;

        $sfiles[filemtime("$path{$s}$file")] = $file;
    }

    if (count($files) != count($sfiles) + 2) throw new Exception('Se pierden archivos.');

    return array_reverse($sfiles);
}
// if ($handle = opendir($location."/2006/01")) {
//     while (false !== ($file = readdir($handle))) {
//
//         if ($file != "." && $file != "..") {
//             $lastModified = date('F d Y, H:i:s',filemtime($location."/2006/01/".$file));
//             echo "$file$lastModified\n";
//
//             // if(strlen($file)-strpos($file,".swf")== 4){
//             // }
//         }
//     }
//     closedir($handle);
// }
