<?php

namespace Tests;

trait RouteProcessTrait
{
    /**
     * Crea un mock de la clase y ejecuta la llamada
     */
    private function __process($classname, $action, $route, $method)
    {
        $drwcont = \Mockery::mock("{$classname}[$method]");
        \App::instance($classname, $drwcont);

        $drwcont->shouldReceive($method)->once();

        $this->call($action, $route);
    }

    /** imprime la ruta a probar */
    private function __log_wrapper($classname, $action, $route, $method)
    {
        echo "$action $route --> $method\n";
        $this->__process($classname, $action, $route, $method);
    }
}
