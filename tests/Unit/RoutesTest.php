<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoutesTest extends TestCase
{
    use \Tests\RouteProcessTrait;

    protected $list = [
        'soats' => [
            // ['GET',     'soats',        'index'],
            ['GET',     'soats/1',        'show'],
            ['GET',     'soats/batch',    'batch'],
            ['POST',    'soats/batch',     'storeBatch'],
            ['GET',     'soats/check/1',   'checkExp'],
        ],
    ];

    public function test_soats()
    {
        $classname = 'App\Http\Controllers\SoatsController';
        $name = 'soats';

        echo "\nAPI: Grupo '$name':\n\n";
        foreach ($this->list[$name] as $row) {
            // code...
            $this->__log_wrapper($classname, ...$row);
        }

        echo "-------------------\n\n";
    }
}
