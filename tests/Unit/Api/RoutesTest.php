<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoutesTest extends TestCase
{
    use \Tests\RouteProcessTrait;

    /** lista de rutas a probar */
    protected $list = [
        'soats' => [
            // ['GET',     'api/soats',      'list'],
            ['GET',   'api/soats/1',        'show'],
            // ['GET',   'api/soats/store',    'form'],
            // ['POST',  'api/soats/store',    'store'],
            ['PUT',   'api/soats/1/update', 'update'],

            ['GET',   'api/soats/check/1',   'checkExp'],
            ['GET',   'api/soats/check/1/alert', 'alertEmail'],
            ['GET',   'api/soats/1/notify',  'notifyExp'],
            // ['GET',   'api/soats/1/renew',   'notifyRenew'],
        ],
        'clients' => [
            ['GET',   'api/clients',  'list'],
            ['GET',   'api/clients/1', 'show'],
            // ['POST',   'api/clients/store', 'store'],
            // ['PUT',   'api/clients/1/update', 'update'],
        ],
        'pol_status' => [
            ['GET',      'api/polstatus',     'list'],
        ],
        'vehicles' => [
            // ['GET',   'api/vehicles',  'list'],
            // ['GET',   'api/vehicles/1', 'show'],
            // ['GET',   'api/vehicles/store', 'form'],
            // ['POST',   'api/vehicles/store', 'store'],
            // ['PUT',   'api/vehicles/1/update', 'update'],
        ],
    ];


    public function test_soats()
    {
        $classname = 'App\Http\Controllers\Api\SoatsController';
        $name = 'soats';

        echo "\nAPI: Grupo '$name':\n\n";
        foreach ($this->list[$name] as $row) {
            // code...
            $this->__log_wrapper($classname, ...$row);
        }

        echo "-------------------\n\n";
    }

    public function test_clients()
    {
        $classname = 'App\Http\Controllers\Api\ClientsController';
        $name = 'clients';

        echo "\nAPI: Grupo '$name':\n\n";
        foreach ($this->list[$name] as $row) {
            // code...
            $this->__log_wrapper($classname, ...$row);
        }

        echo "-------------------\n\n";
    }

    public function test_pol_status()
    {
        $classname = 'App\Http\Controllers\Api\PolStatusController';
        $name = 'pol_status';

        echo "\nAPI: Grupo '$name':\n\n";
        foreach ($this->list[$name] as $row) {
            // code...
            $this->__log_wrapper($classname, ...$row);
        }

        echo "-------------------\n\n";
    }
}
