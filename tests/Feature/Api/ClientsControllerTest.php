<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientsControllerTest extends TestCase
{
    /**
     * obtiene 203 si el cliente no se encuentra
     * @covers show
     */
    public function test_nonexistent_client()
    {
        $res = $this->call('GET', route('api.clients.show', 99999) );

        $res->assertStatus(203);
    }

    /**
     * obtiene un cliente sin más atributos
     * @covers show
     */
    public function test_get_client()
    {
        $res = $this->call('GET', route('api.clients.show', 1) );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'name',
            'type',
            'doc',
        ]);
    }

    /**
     * obtiene un cliente con todas las pólizas
     * @covers show
     */
    public function test_get_expanded_client()
    {
        $res = $this->call('GET', route('api.clients.show', '1?with=expanded,soats') );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'clientable',
            'emails',
            'phones',
            'addresses',
            'type',
            'soats',
        ]);
    }

    /**
     * obtiene todos los clientes
     *
     * @covers list
     */
    public function test_get_all_clients()
    {
        $res = $this->call('GET', route('api.clients.list') );

        $res->assertStatus(200);

        // data no está vacío
        $this->assertNotEmpty($res->json());

        $row = $res->json()[0];

        $this->assertTrue(array_key_exists('id', $row));
        $this->assertTrue(array_key_exists('name', $row));
        $this->assertTrue(array_key_exists('doc', $row));
        $this->assertTrue(array_key_exists('type', $row));
    }

    /**
     * obtiene porción los clientes paginados
     *
     * @covers list
     */
    public function test_get_all_clients_paginated()
    {
        $res = $this->call('GET', route('api.clients.list', 'page=1') );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'current_page',
            'data',
            'total',
        ]);
        // data no está vacío
        $this->assertNotEmpty($res->json()['data']);
    }
}
