<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Api\SoatsController;
use App\Mail\SoatsAlert;
use App\Mail\Admin\SoatsAlert as AdminMail;
use App\Soat;

class SoatsControllerTest extends TestCase
{
    /**
     * obtiene 203 si el soat no se encuentra
     * @covers show
     */
    public function test_nonexistent_soat()
    {
        $res = $this->call('GET', route('api.soats.show', 99999) );

        $res->assertStatus(203);
    }

    /**
     * obtiene un soat sin más atributos
     * @covers show
     */
    public function test_get_soat()
    {
        $res = $this->call('GET', route('api.soats.show', 1) );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'created',
            'from',
            'exp',
            'amount',
            'patent',
            'use'
        ]);
    }

    /**
     * obtiene un soat con todas las pólizas
     * @covers show
     */
    public function test_get_expanded_soat()
    {
        $res = $this->call('GET', route('api.soats.show', '1?with=expanded') );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'client',
            'vehicle',
            'class',
            'insurer',
        ]);
    }

    /**
     * obtiene todos los soat
     *
     * @covers list
     */
    public function test_get_all_soats()
    {
        $res = $this->call('GET', route('api.soats.list') );

        $res->assertStatus(200);

        // data no está vacío
        $this->assertNotEmpty($res->json());

        $row = $res->json()[0];

        $this->assertTrue(array_key_exists('exp', $row));
        $this->assertTrue(array_key_exists('patent', $row));
        $this->assertTrue(array_key_exists('use', $row));
        $this->assertTrue(array_key_exists('amount', $row));
        $this->assertTrue(array_key_exists('client', $row));
        $this->assertTrue(array_key_exists('vehicle', $row));
    }

    /**
     * obtiene fracción de soats paginados
     *
     * @covers list
     */
    public function test_get_all_soats_paginated()
    {
        $res = $this->call('GET', route('api.soats.list', 'page=1') );

        $res->assertStatus(200);

        $res->assertJsonStructure([
            'current_page',
            'data',
            'total',
        ]);
        // data no está vacío
        $this->assertNotEmpty($res->json()['data']);
    }

    /**
     * Prueba los meses de vencimiento
     *
     * @covers checkExp
     */
    public function test_check($months=null)
    {
        $res = $this->call('GET', route('api.soats.check', $months));

        if (!empty($data = $res->json())) {
            $res->assertStatus(200);

            $this->assertTrue(array_key_exists('type', $data[0]));
            $this->assertTrue(array_key_exists('soatid', $data[0]));
            $this->assertTrue(array_key_exists('clientid', $data[0]));
            $this->assertTrue(array_key_exists('fullname', $data[0]));
            $this->assertTrue(array_key_exists('patent', $data[0]));
            $this->assertTrue(array_key_exists('exp', $data[0]));
            $this->assertTrue(array_key_exists('brand', $data[0]));
            $this->assertTrue(array_key_exists('total', $data[0]));
            $this->assertTrue(array_key_exists('sms', $data[0]));
            $this->assertTrue(array_key_exists('gender', $data[0]));
            $this->assertTrue(array_key_exists('status', $data[0]));
        } else {
            $res->assertStatus(203);
        }
    }

    /**
     * @covers checkExp
     */
    public function test_check_empty()
    {
        $this->test_check(0);
    }

    /**
     * @covers checkExp
     */
    public function test_check_2_months()
    {
        $this->test_check(2);
    }

    /**
     * @covers notifyExp
     */
    public function test_notify_exp()
    {
        $c = new SoatsController;
        $soatids = [1, 2, 999];

        echo "\nProbando SoatsController@notifyExp:\n";

        foreach ($soatids as $id) {
            Mail::fake();
            $res = $c->notifyExp($id);

            echo "\nenviando a: $id ";
            if ($soat= Soat::find($id)) {
                $client = SoatsController::createClient($soat);

                if ($user = \App\Email::find($client->emailid)) {
                    Mail::assertSent(SoatsAlert::class, function ($mail) use ($user) {
                        return $mail->hasTo($user->email);
                    });
                    echo "enviado.";
                } else {
                    echo "no enviado.";
                    Mail::assertNotSent(SoatsAlert::class);
                }
            } else {
                echo "no existe.";
                $this->assertNull($res);
                Mail::assertNotSent(SoatsAlert::class);
            }
        }
        echo "\n------------------\n";
    }

    /**
     * @covers alertEmail
     */
    public function test_alert_email()
    {
        $months = 3;
        Mail::fake();
        $c = new SoatsController;
        $res = $c->alertEmail($months);
        $count = count($res);

        echo "\nAlertEmail meses: $months total: $count vencimientos.\n";

        Mail::assertSent(AdminMail::class, function ($mail) {
            return $mail->hasTo('villalvarseguros@gmail.com');
        });
    }
}
