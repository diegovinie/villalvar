## Villalvar
Villalvar es una aplicación web para la administración de Villalvar Seguros Ltda. Está basada en [Laravel](http://laravel.com) con componentes en [Vue.js](http://vuejs.org) que se comunican con el servidor a través de una API.

### Instalación:
```sh
git clone https://gitlab.com/diegovinie/villalvar.git
cd villalvar
composer install
npm install
npm run dev
```

renombrar `.env-example` como `.env`

En el archivo `.env` colocar los datos correspondientes a la conexión de base de
datos y a la cuenta de correo electrónico.


Generar la clave del producto:
```sh
php artisan key:generate
```


Preparar la base de datos:
```sh
php artisan migrate --seed
```

### Uso:

### Servidor integrado:
```sh
php artisan serve
```


### Tareas programadas:

##### Tareas activas:
- Envía correo diario con la lista de Soats a vencer.
- Envía correos diarios por cada cliente que se le vence el Soat en ese día.

Agregar al `crontab`:
```sh
* * * * * php /ruta/al/proyecto/artisan schedule:run
```


#### Soats:

Para introducir un archivo por lotes a la base de datos de Soats po la 
siguiente ruta:
`GET /soats/batch`

Para ver soats próximos a vencer:
`GET /soats/check/(nro. meses)`
