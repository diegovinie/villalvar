<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soats', function (Blueprint $table) {
            $table->increments('id');
            $table->date('created')->nullable();    // día de expedición
            $table->date('from')->nullable();   // inicio vigencia
            $table->date('exp')->index();    // vencimiento
            $table->decimal('amount', 10, 2)->nullable(); // pagado
            $table->unsignedInteger('insurer_id');  // Compañía aseguradora
            $table->integer('ct')->nullable();  // Cod Type
            $table->string('use')->nullable();  // particular, publico
            $table->boolean('expired')->nullable(); // si está vencido
            $table->boolean('renewed')->default(false); // si fue renovado
            $table->unsignedInteger('client_id');   // Cliente
            $table->unsignedInteger('vehicle_id');  // vehículo
            $table->string('patent')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
            $table->foreign('insurer_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soats');
    }
}
