<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('cc')->unique();       // cédula
            $table->string('name1')->nullable();
            $table->string('name2')->nullable();
            $table->string('lastname1')->index();    // Obligatorio un apellido
            $table->string('lastname2')->nullable();
            $table->date('birth')->nullable();  // Nacimiento
            $table->string('gender')->nullable();   // Género
            $table->string('occupation')->nullable();
            $table->decimal('income')->default(0);  // Ingresos
            // Tipo de relacion: 1. ninguna, 2. asegurador, 3. cliente, 4. intermediario, 5. trabajador
            $table->unsignedInteger('rel')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
