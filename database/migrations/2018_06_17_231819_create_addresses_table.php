<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Direcciones para personas y organizaciones
         */
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address')->default('N/S');
            $table->unsignedInteger('client_id');
            $table->string('department')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
