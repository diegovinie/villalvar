<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkGeneralesGenTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generales', function (Blueprint $table) {
            //
            $table->foreign('type')->references('id')->on('gen_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generales', function (Blueprint $table) {
            $table->dropForeign('generales_type_foreign');
        });
    }
}
