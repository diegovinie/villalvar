<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('pol_status', function (Blueprint $table) {

             // 1	Pagado
             // 2	Entregado
             // 3	Expedido
             // 4	Proceder
             // 5	En consideración
             // 6	Notificado (correo)
             // 7	Notificado (sms)
             // 8	Notificado (correo, sms)
             // 9	No renovar
             $table->increments('id');             
             $table->string('name');
             $table->json('misc')->nullable();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         // Schema::disableForeignKeyConstraints();
         Schema::dropIfExists('pol_status');
     }
}
