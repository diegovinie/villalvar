<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Vehículos: motos, carros, etc.
         */
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patent')->unique();       // placas
            $table->string('use')->nullable();  // particular, publico
            $table->integer('cil')->nullable(); // cilindraje
            $table->integer('model');   // año del carro
            $table->string('brand');    // Marca
            $table->string('class')->nullable(); //Descripción
            $table->unsignedInteger('ct')->nullable();   // Cod Type Soat
            $table->unsignedInteger('weight')->nullable(); // Peso en TON
            $table->unsignedInteger('passengers')->nullable(); // pasajeros
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
