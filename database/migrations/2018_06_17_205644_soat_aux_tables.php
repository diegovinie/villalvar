<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoatAuxTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * Tabla para calcular el valor de renovación del Soat
         */
        Schema::create('soat_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company')->nullable();  // compañía que asegura
            $table->integer('year');       // año de vigencia
            $table->integer('ct');      // Cod Type
            $table->integer('crate');   // Cod Rate
            $table->integer('mod')->default(0); // generalmente la antiguedad
            $table->decimal('prima', 10, 2);
            $table->decimal('total', 10, 2);    // valor con impuestos
        });

        /**
         * Tabla para identificar los Cod Type
         */
        Schema::create('soat_class', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ct');      // Cod Type
            $table->string('name');

        });

        /**
         * Descripción Soat Cod Type
         */
        Schema::create('soat_desc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
            $table->unsignedInteger('ct');
            $table->integer('crate');
            $table->string('unit')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();

            $table->foreign('ct')->references('id')->on('soat_class')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soat_desc');
        Schema::dropIfExists('soat_class');
        Schema::dropIfExists('soat_rates');
            }
}
