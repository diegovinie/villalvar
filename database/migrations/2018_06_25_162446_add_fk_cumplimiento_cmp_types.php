<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCumplimientoCmpTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cumplimiento', function (Blueprint $table) {
            //
            $table->foreign('type')->references('id')->on('cmp_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cumplimiento', function (Blueprint $table) {
            $table->dropForeign('cumplimiento_type_foreign');
        });
    }
}
