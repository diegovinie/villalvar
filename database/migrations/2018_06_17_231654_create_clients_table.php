<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Clientes. Pueden ser personas o compañías
         */
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();     // nombre corto
            $table->string('clientable_type');      // clase (people, companies)
            $table->unsignedInteger('clientable_id');  // id en ref
            $table->unsignedInteger('phone_id')->nullable(); // número principal
            $table->unsignedInteger('email_id')->nullable(); // correo principal
            $table->unsignedInteger('address_id')->nullable(); // dirección principal
            $table->unsignedInteger('doc')->nullable();
            $table->string('type')->nullable();
            $table->integer('qt')->nullable();  // valoración
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
