<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusInSoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soats', function (Blueprint $table) {
            $table->dropColumn('status');

            $table->unsignedInteger('status_id')
                ->after('use')
                ->nullable();

            $table->foreign('status_id')->references('id')->on('pol_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soats', function (Blueprint $table) {
            if (Schema::hasColumn('soats', 'status_id')) {
                // code...
                $table->dropForeign('soats_status_id_foreign');
                $table->dropColumn('status_id');
            }
        });
    }
}
