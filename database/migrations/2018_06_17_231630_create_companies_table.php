<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Empresas
         */
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('nit')->unique();
            $table->string('name')->index();
            $table->string('www')->nullable();
            $table->string('business')->nullable();     // tipo de negocio
            // Tipo de relacion: 1. ninguna, 2. clave, 3. cliente, 4. intermediario, 5. trabajador
            $table->unsignedInteger('rel')->default(1);
            $table->json('misc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
