<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToSoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soats', function (Blueprint $table) {
            //
            $table->string('status')->after('use')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soats', function (Blueprint $table) {
            //
            if (Schema::hasColumn('soats', 'status')) {
                $table->dropColumn('status');
            }
        });
    }
}
