<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCumplimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cumplimiento', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->unsignedInteger('taker');
            $table->unsignedInteger('insured')->nullable();
            $table->unsignedInteger('beneficiary')->nullable();
            $table->unsignedInteger('secured')->nullable();
            $table->unsignedInteger('insurer');
            $table->unsignedInteger('type');
            $table->date('created')->nullable();
            $table->unsignedInteger('number')->nullable();
            $table->unsignedInteger('annexed')->nullable();
            $table->string('status')->nullable();
            $table->decimal('prime', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->decimal('comission', 10, 2)->nullable();
            $table->boolean('expired')->nullable();
            $table->boolean('renewed')->nullable();
            $table->json('misc')->nullable();

            $table->foreign('taker')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('insured')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('beneficiary')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('secured')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('insurer')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cumplimiento');
    }
}
