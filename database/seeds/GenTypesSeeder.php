<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class GenTypesSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'gen_types';
        $this->filename = base_path().'/database/data/csvs/gen_types.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table($this->table)->truncate();
        parent::run();
    }
}
