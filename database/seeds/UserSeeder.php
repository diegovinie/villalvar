<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = App\User::create([
            'email' => 'diegovinie@gmail.com',
            'role' => 'client',
        ]);

        $admin = App\User::create([
            'email' => 'diego.viniegra@gmail.com',
            'role' => 'admin',          
        ]);
    }
}
