<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class SoatClassSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'soat_class';
        $this->filename = base_path().'/database/data/csvs/soat_class.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table($this->table)->truncate();
        parent::run();
    }
}
