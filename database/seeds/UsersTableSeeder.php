<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'email' => 'villalvarseguros@gmail.com',
            'role' => 'admin', 
        ]);

        App\User::create([
            'email' => 'diegovinie@gmail.com',
            'role' => 'client',
        ]);

        App\User::create([
            'email' => 'diego.viniegra@gmail.com',
            'role' => 'admin',
        ]);
    }
}
