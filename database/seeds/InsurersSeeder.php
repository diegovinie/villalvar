<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Flynsarmy\CsvSeeder\CsvSeeder;

class InsurersSeeder extends Seeder
{
    public function __construct()
    {
        $this->table = 'companies';
        $this->filename = base_path().'/database/data/csvs/insurers.csv';
        // $this->mapping = [
        //     0 => 'id',
        //     1 =>  'name',
        //     2 =>  'nit',
        //     3 =>  'address',
        //     4 =>  'city',
        //     5 => 'www',
        //     6 => 'rel'
        // ];
        // $this->offset_rows = 1;
    }

    public function convertCsvArray(/*resource*/ $handle)
        {

        $data = array();
        $keys = fgetcsv($handle); // lee la primera fila (encabezado) y la guarda como un array

        while(!feof($handle)){

            $values = fgetcsv($handle);  // lee la siguiente línea
            $row = array();
            $i = 0;

            if(!$values) break;

            // expande los valores leídos haciéndolos coincidir con la respectiva cabecera
            for($i = 0; $i < count($keys); $i++){

                $row[$keys[$i]] = $values[$i];
            }
            // agregamos la fila al array
            $data[] = $row;
        }

        return $data;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen($this->filename, 'r');
        $data = $this->convertCsvArray($handle);

        foreach ($data as $row) {

            DB::table('companies')->insert([
                'nit' => preg_replace("/,|\.|-|_|/", "", $row['NIT']),
                'name' => $row['NOMBRE'],
                'www' => $row['SITE'],
                'business' => 'ASEGURADORA',
                'rel' => $row['REL'],
                'misc' => "{\"address\": \"{$row['DIRECCION']}\", \"city\": \"{$row['CIUDAD']}\"}",
            ]);
        }
    }
}
