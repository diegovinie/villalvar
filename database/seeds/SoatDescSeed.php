<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class SoatDescSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'soat_desc';
        $this->filename = base_path().'/database/data/csvs/soat_desc.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table($this->table)->truncate();
        parent::run();
    }
}
