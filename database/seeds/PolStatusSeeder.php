<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class PolStatusSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function __construct()
     {
         $this->table = 'pol_status';
         $this->filename = base_path().'/database/data/csvs/pol_status.csv';
     }

     /**
      * Run the database seeds.
      *
      * @return void
      */
     public function run()
     {
         // DB::table($this->table)->truncate();
         parent::run();
     }
}
