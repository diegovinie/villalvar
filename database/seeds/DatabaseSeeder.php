<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(InsurersSeeder::class);
        $this->call(SoatClassSeeder::class);
        $this->call(SoatDescSeeder::class);
        $this->call(SoatRatesSeeder::class);
        $this->call(CmpTypesSeeder::class);
        $this->call(GenTypesSeeder::class);
        $this->call(PolStatusSeeder::class);
        // $this->call(UserSeeder::class);
    }
}
