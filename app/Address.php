<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    protected $hidden = [
        'created_at',   'updated_at',
        'client_id',
    ];

    public static function saveAddress($newAddress, Client $client)
    {
        if (!$address = Address::where('address', $newAddress)->first()) {

            $address = new Address;

            $address->address = $newAddress? $newAddress : '';
            $address->client_id = $client->id;

            $address->save();

            return $address;
        }
    }
}
