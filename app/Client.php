<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $hidden = [
        'created_at',       'updated_at',
        'clientable_id',    'clientable_type',
        'email_id',         'address_id',
    ];

    /************* Relaciones *******************************/

    /**
     * El modelo de Person o Company
     */
    public function clientable()
    {
        return $this->morphTo();
    }

    /**
     * Une con tabla soats
     */
    public function soats()
    {
        return $this->hasMany('App\Soat');
    }

    /**
    * Los emails
    */
    public function emails()
    {
        return $this->hasMany('App\Email', 'id');
    }

    /**
     * Los tel del cliente
     */
    public function phones()
    {
        return $this->hasMany('App\Phone', 'id');
    }

    /**
     * Los direcciones del cliente
     */
    public function addresses()
    {
        return $this->hasMany('App\Address', 'id');
    }

    /**************** Dinámicas *****************************/

    /**
     * Devuelve la cc si es persona o el nit si es empresa
     */
    public function getDocAttribute()
    {
        if ($this->clientable_type == 'App\Person') {
            return Person::find($this->clientable_id)->cc;

        } else if ($this->clientable_type == 'App\Company') {
            return Company::find($this->clientable_id)->nit;

        } else {
            return null;
        }
    }

    /**
     * Devuelve el tipo de cliente para el front
     */
    public function getTypeAttribute()
    {
      if ($this->clientable_type == 'App\Person') {
          return 'natural';

      } else if ($this->clientable_type == 'App\Company') {
          return 'juridico';

      } else {
          return null;
      }
    }

    /**
     * Crea un cliente persona
     *
     * @param \App\Person $person
     * @return \App\Client
     */
    public static function savePerson(Person $person)
    {
        $client = new Client;

        $client->clientable_type = 'App\Person';
        $client->clientable_id = $person->id;
        $client->name = $person->name1 .' ' .$person->lastname1;

        $client->save();

        return $client;
    }

    /*************** Estáticas *****************************/

    /**
    * Crea un cliente empresa
    *
    * @param \App\Company $company
    * @return \App\Client
    */
    public static function saveCompany(Company $company)
    {
        $client = new Client;

        $client->clientable_type = 'App\Company';
        $client->clientable_id = $company->id;
        $client->name = $company->name;

        $client->save();

        return $client;
    }
}
