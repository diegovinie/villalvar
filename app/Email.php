<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $hidden = [
        'created_at',   'updated_at',
        'client_id',
    ];

    /**
     * Guarda un correo y lo asocia con un cliente
     *
     * @param string $newEmail
     * @param \App\Client $client
     * @return \App\Email
     */
    public static function saveEmail($newEmail, Client $client)
    {
        // verificar si es correo-e
        if ($e = filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {

            if (!$email = Email::where('email', $newEmail)->where('client_id', $client->id)->first()) {

                $email = new Email;

                $email->email = $newEmail;
                $email->client_id = $client->id;

                $email->save();
            }

            return $email;
        }
    }
}
