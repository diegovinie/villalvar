<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SoatsAlert extends Mailable
{
    use Queueable, SerializesModels;

    public $soats;
    public $months;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($soats, $months)
    {
        $this->from[] = [
            'address' => 'cerebro@villalvar.com',
            'name'    => 'Señor Cerebro'
        ];
        $this->soats = $soats;
        $this->months = $months;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.admin.soatsAlert')
                    ->subject('Seguimiento de Vencimiento de Soats');
    }
}
