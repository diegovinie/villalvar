<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Soat;
use App\Client;
use App\Vehicle;

class NotifyRenewMail extends Mailable
{
    use Queueable, SerializesModels;

    public $soat;
    public $client;
    public $vehicle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Soat $soat, Client $client, Vehicle $vehicle)
    {
        $this->from[] = [
            'address' => 'cerebro@villalvar.com',
            'name'    => 'Señor Cerebro'
        ];
        $this->soat = $soat;
        $this->client = $client;
        $this->vehicle = $vehicle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.admin.notifyRenew')
            ->subject("Soat: {$this->client->name} desea renovar");
    }
}
