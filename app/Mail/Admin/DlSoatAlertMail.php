<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DlSoatAlertMail extends Mailable
{
    use Queueable, SerializesModels;

    public $client;
    public $soat;
    public $vehicle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($soat, $client, $vehicle)
    {
        $this->client = $client;
        $this->soat = $soat;
        $this->vehicle = $vehicle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.admin.dlsoatalert')
            ->subject('Soat - Vencimiento inminente!');
    }
}
