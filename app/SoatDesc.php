<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoatDesc extends Model
{
    //
    protected $table = 'soat_desc';

    public function soatClass()
    {
        return $this->hasOne('App\SoatClass', 'ct');
    }
}
