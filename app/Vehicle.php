<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * Crea un registro cuando los datos viene de soat/batch
     *
     * @return \App\Vehicle
     */
    public static function setData($row)
    {
        if (!$vehicle = Vehicle::where('patent', $row['patent'])->first()) {

            $vehicle = new Vehicle;

            $vehicle->patent = $row['patent'];
            $vehicle->use = $row['use'];
            $vehicle->cil = $row['cil'];
            $vehicle->model = $row['year'];
            $vehicle->brand = $row['brand'];
            $vehicle->class = SoatClass::find($row['class'])->name;
            $vehicle->ct = $row['ct'];

            $vehicle->save();
        }
        return $vehicle;
    }
}
