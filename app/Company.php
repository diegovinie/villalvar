<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * Crea un registro cuando los datos viene de soat/batch
     *
     * @return \App\Company
     */
    public static function setData($row)
    {
        if ($company = Company::where('nit', $row['nit'])->first()) {
            return $company;
        }
        else {
            $company = new Company;

            $company->nit = $row['nit'];
            $company->name = $row['name'];
            $company->rel = 3; // cliente

            $company->save();

            return $company;
        }
    }

    public function client()
    {
        return $this->morphMany('App\Client', 'clientable');
    }
}
