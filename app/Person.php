<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    /**
     * Crea un registro cuando los datos viene de soat/batch
     *
     * @return \App\Person
     */
    public static function setData($row)
    {
        // verifica si ya existe
        if ($person = Person::where('cc', $row['cc'])->first()) {
            return $person;
        }
        else {
            $person = new Person;

            $person->name1 = $row['name1'];
            $person->name2 = $row['name2'];
            $person->lastname1 = $row['lastname1'];
            $person->lastname2 = $row['lastname2'];
            $person->cc = $row['cc'];
            $person->gender = $row['gen'];
            $person->rel = 3; // cliente

            $person->save();
            return $person;
        }
    }

    public function client()
    {
        return $this->morphMany('App\Client', 'clientable');
    }
}
