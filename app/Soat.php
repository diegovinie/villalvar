<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soat extends Model
{
    protected $hidden = [
        'created_at',   'updated_at',
        'client_id', 'vehicle_id', 'insurer_id'
    ];


    /************* Relaciones *******************************/

    /**
     * Une con tabla pol_status
     */
    public function status()
    {
        return $this->belongsTo('App\PolStatus', 'status_id');
    }

    /**
     * Une con tabla clients
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * Une con tabla vehicles
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    /**
     * Une con tabla soat_class
     */
    public function class()
    {
        return $this->belongsTo('App\SoatClass', 'ct');
    }

    /**
     *
     */
    public function insurer()
    {
        return $this->belongsTo('App\Company', 'insurer_id', 'id');
    }

    /**************** Dinámicas *****************************/

    /**
     * Informa si el Soat se venció
     *
     * @return bool
     */
    public function getExpiredAttribute()
    {
        $today = new \DateTime;
        $expDate = new \Datetime($this->exp);

        return $expDate < $today;
    }

    public function getPatentAttribute()
    {
        return Vehicle::find($this->vehicle_id)->patent;
    }

    public function getInsurerAttribute()
    {
        return Company::find($this->insurer_id);
    }

    /*************** Estáticas *****************************/

    /**
     * Cambia a renovado
     */
    public static function renewed($id)
    {
        $soat = Soat::find($id);

        if (!$soat) return false;

        $soat->renewed = true;

        $soat->save();

        return true;
    }
}
