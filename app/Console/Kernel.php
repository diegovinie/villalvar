<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->everyMinute();

        /**
         * Revisa los soats que están por vencer en 1 mes
         */
        $schedule->call(function () {
            $ctr = new \App\Http\Controllers\Api\SoatsController;
            $ctr->alertEmail(1);
        })
        ->dailyAt('8:00');
        // ->everyMinute();

        /**
         * Revisa los soats que se vencen hoy y mañana
         */
        $schedule->call(function () {
            $ctr = new \App\Http\Controllers\Api\SoatsController;
            $ctr->deathlineCheck();
        })
        ->dailyAt('8:30');
        // ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
