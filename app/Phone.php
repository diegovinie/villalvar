<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $hidden = [
        'created_at',   'updated_at',
        'client_id',
    ];

    /**
     * Guarda un teléfono asociado a un cliente
     *
     * @param int $number
     * @param \App\Client $client
     * @return \App\Phone
     */
    public static function savePhone($number, Client $client)
    {
        if (!$phone = Phone::where('number', $number)->where('client_id', $client->id)->first()) {

            $phone = new Phone;

            $phone->number = $number? $number : '';
            $phone->client_id = $client->id;
            $phone->sms = strlen($number) == 10 && $number[0] == '3'? true : false;
            $phone->local = (function ($number) {
                return strlen($number) < 10 && strlen($number) > 6? true : false;
            })($number);

            $phone->save();
        }
        return $phone;
    }
}
