<?php

namespace App\Http\Controllers\Mch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mch\Producto;

class HomeController extends Controller
{
    //
    public function index ()
    {
      $menuItems = $this->getMenuItems();

      return view('public.home')->with(
        compact(
          'menuItems'
          )
      );
    }

    public function product ($id)
    {
      $menuItems = $this->getMenuItems();

      $product = Producto::find($id);

      return view('public.product')->with(
        compact(
          'product',
          'menuItems'
          )
      );
    }

    public function contact ()
    {
      $menuItems = $this->getMenuItems();



      return view('public.contact')->with(
        compact(
          'menuItems'
          )
      );

    }

    public function socialLogin()
    {
        $menuItems = $this->getMenuItems();

        return view('partials.social')->with(
          compact(
            'menuItems'
            )
        );
    }

    protected function getMenuItems ()
    {
      $proPer = Producto::whereGrupo('p')->whereEstado('a')->get();

      $proEmp = Producto::whereGrupo('e')->whereEstado('a')->get();

      $proVeh = Producto::whereGrupo('v')->whereEstado('a')->get();

      return compact(
        'proPer',
        'proEmp',
        'proVeh'
      );
    }
}
