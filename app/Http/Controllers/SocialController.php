<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
    public function redirect ($provider)
    {
        try {
            return Socialite::with($provider)->redirect();

        } catch (\Exception $e) {
            return view('login');
        }

    }

    public function handle ($provider)
    {
        $socialUser = Socialite::with($provider)->user();

        $user = User::where('email', '=', $socialUser->getEmail())->first();

        if (!$user) {
            return redirect()->route('register')->with([
                'status' => 'success',
                'message' => 'Por favor regístrese.',
                'email' => $socialUser->getEmail(),
            ]);
        }

        if (!$user->name) {
            $user->name = $socialUser->getName();
        }

        if (!$user->avatar) {
            $user->avatar = $socialUser->getAvatar();
        }

        $token_exp = new \DateTime;

        $user->session_exp = $token_exp->add(new \DateInterval('PT8M'));
        $user->save();

        Auth::login($user);

        if (Auth::user()->role === 'admin') {
            return redirect()->route('clients')->cookie('user', $user->email, 30);
        }

        return redirect()->route('users')->cookie('user', $user->email, 30);
    }
}
