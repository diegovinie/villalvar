<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Email;
use App\Phone;
use App\Address;
use App\Soat;
use App\Vehicle;
use App\Client;
use App\Company;

class SoatsController extends Controller
{
    public function list()
    {
        return view('soats.index');
    }

    public function show($id)
    {
        return view('soats.soat')->with(compact('id'));
    }

    /**
     * Devuelve el formulario para registrar un soat por renovación
     */
    public function renewform($id)
    {
        $s = new \stdClass;
        $s->hola = 'mundo';
        $soat = Soat::with('client', 'vehicle', 'insurer', 'class', 'status')->find($id);
        // echo response()->json($soat);die;

        return view('soats.renew')->with(['soat' => json_encode($s)]);
    }

    /**
     * Envía correo al operador indicando que el cliente desea renovar
     *
     * Actualiza el status_id
     * Redirige al front con thanks
     */
    public function notifyRenew(int $soatid)
    {
        $soat = Soat::find($soatid);
        $client = Client::find($soat->client_id);
        $vehicle = Vehicle::find($soat->vehicle_id);
        $email = Email::find($client->email_id)->email;

        Mail::to(TEST_EMAIL_ADMIN)->send(new NotifyRenewMail($soat, $client, $vehicle));
        // return new NotifyRenewMail($soat, $client, $vehicle);
        $soat->status_id = 4; // proceder
        $soat->save();

        return redirect()->route('thanks');
    }

    /**
     * Muestra el formulario para almacenar pila de Soats
     */
    public function batch()
    {
        return view('soats.batch');
    }

    /**
     * Almacena una pila de Soats
     */
    public function storeBatch(Request $request)
    {
        // reorganiza en un array
        $aData = $this->prepareData( $request->all() );

        foreach ($aData as $index => $row) {
            // aclarar el tipo de cliente
            $type = $row['type'] == 'natural'? 'App\Person' : 'App\Company';

            // la persona
            if ($type == 'App\Person') {
                // verifica si ya existe
                $person = Person::setData($row);
                $idPerCo = $person->id;
            }

            // la empresa
            if ($type == 'App\Company') {
                $company = Company::setData($row);
                $idPerCo = $company->id;
            }

            // incluir cliente
            // verifica si existe
            if (!$client = Client::where('clientable_type', $type)->where('clientable_id', $idPerCo)->first()) {
                // continua
                $client = $type == 'App\Person'? Client::savePerson($person) : Client::saveCompany($company);
            }

            // incluir telefonos
            if (isset($row['phones'])) {
                foreach ($row['phones'] as $phoneNumber) {
                    // chequeo para fijar el principal
                    $check = false;
                    $phone = Phone::savePhone($phoneNumber, $client);
                    $client->phone_id = $phone->id;
                    $client->save();

                    if (!$check) {
                        // actualizar cliente.phone $phone->id
                        $check = true;
                    }
                }
            }

            // incluir correos-e
            if (isset($row['emails'])) {
                foreach ($row['emails'] as $emailAddress) {
                    // chequeo para fijar el principal
                    $check = false;
                    if (filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
                        $email = Email::saveEmail($emailAddress, $client);
                        $client->email_id = $email->id;
                        $client->save();

                    }

                    if (!$check) {
                        // actualizar cliente.email $phone->id
                        $check = true;
                    }
                }
            }

            // incluir direcciones
            // verificar si existe
            $address = Address::saveAddress($row['address'], $client);


            // incluir el vehículo
            // verificar si existe
            $vehicle = Vehicle::setData($row);

            // incluir el soat
            $soat = new Soat;

            $date = preg_replace("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", "$3-$2-$1", $row['exp']);

            $soat->exp = $date;
            $soat->amount = 0;
            $soat->insurer_id = 2;  // seguros del estado
            $soat->client_id = $client->id;
            $soat->vehicle_id = $vehicle->id;
            $soat->ct = $row['ct'];
            $soat->use = $row['use'];

            $soat->save();

        }
        return view('soats.batch')->with(['success' => 'Almacenado.']);
    }

    /**
     * Devuelve un array multidimensional con los datos de la pila
     *
     * @param array $rawData unidimensional
     * @return array multidimensional
     */
    protected function prepareData($rawData)
    {
        // Los datos organizados por fila
        $aData = [];

        // Construcción del array $aData
        foreach ($rawData as $key => $value) {
            if (strpos($key, '_')) {
                // code..
                list($row, $name) = preg_split("/_/", $key);

                if (strpos($name, '-')) {

                    list($name2, $ind) = preg_split("/-/", $name);
                    $aData[$row][$name2][$ind] = $value;

                } else {
                    $aData[$row][$name] = $value;
                }
            }
        }
        return $aData;
    }

    /**
     * Muestra la vista con los Soats por vencer
     *
     * @param int $months n de meses en los que se vence el soat
     */
    public function checkExp($months)
    {
        return view('soats.check')->with(['months' => $months]);
    }
}
