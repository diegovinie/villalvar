<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index()
    {
        /**
         * Muestra todos los clientes
         */
        return view('clients.index');
    }

    /**
     * Muestra un cliente
     *
     * @param int $id id del cliente
     */
    public function show($id)
    {
        return view('clients.show')->with(compact('id'));
    }
}
