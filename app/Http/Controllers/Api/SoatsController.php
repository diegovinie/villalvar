<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Person;
use App\Email;
use App\Phone;
use App\Address;
use App\Soat;
use App\Vehicle;
use App\Client;
use App\Company;
use App\SoatDesc;
use \App\SoatRate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Mail;
use App\Mail\SoatsAlert;
use App\Mail\Admin\SoatsAlert as AdminMail;
use App\Mail\Admin\NotifyRenewMail;
use App\Mail\Admin\DlSoatAlertMail;

const TEST_EMAIL_ADMIN = 'villalvarseguros@gmail.com';

/**
 * Operaciones con los soat a nivel de API
 */
class SoatsController extends Controller
{

    /********************** RUTAS *********************************/

    /**
     * Manda Mail al correo del administrador
     *
     * @param int $months
     * @return json la lista de los involucrados
     */
    public function alertEmail($months)
    {
        $list = SoatsController::checkExp($months);
        Mail::to(TEST_EMAIL_ADMIN)->send(new AdminMail($list, $months));

        return response()->json($list);
    }

    /**
     * Devuelve json con los datos de un soat
     *
     * @param int $id número del soat
     * @return json datos del soat
     */
    public function show(int $id)
    {
        $props = [];

        if($att = Input::get('with')) {
            $att = explode(',', $att);

            if (in_array('expanded', $att)) {
                $props = array_merge($props, [
                    'client', 'vehicle',
                    'status', 'class',
                    'insurer'
                ]);
            }
        }

        if (!$soat = Soat::with($props)->find($id)) {
            return response()->json(null, 203);
        } else {
            return $soat;
        }
    }

    public function list()
    {
      if (Input::get('page')) {
          return Soat::with('client', 'vehicle')->paginate(10);
      } else {
          return Soat::with('client', 'vehicle')->get();
      }
    }

    /**
     * Manda correo al administrador de los soats que se vencen hoy y mañana
     */
    public function deathlineCheck()
    {
        $today = new \DateTime;
        $tmw = $today->modify('+1 day');

        $todayf = $today->format('Y-m-d');
        $tmwf = $tmw->format('Y-m-d');

        $soats = Soat::where('exp', '>=', $todayf)
            ->where('exp', '<=', $tmwf)
            ->get();

        foreach ($soats as $soat) {
            $vehicle = Vehicle::find($soat->vehicle_id);
            $client = SoatsController::createClient($soat);

            Mail::to(TEST_EMAIL_ADMIN)->send(new DlSoatAlertMail($soat, $client, $vehicle));
        }
    }
    /**
     * Para probar si está mandando emails
     */
    public function mailxx()
    {
        echo Mail::to(TEST_EMAIL_ADMIN)->send(new AdminMail([], 2));
    }

    public function test()
    {
        $soat = Soat::find(2);
        // return $soat;
        $vehicle = Vehicle::find($soat->vehicle_id);

        $client = SoatsController::createClient($soat);
        // var_dump($client);die;
        Mail::to(TEST_EMAIL_ADMIN)->send(new \App\Mail\Admin\DlSoatAlertMail($soat, $client, $vehicle));
        // return new \App\Mail\Admin\DlSoatAlertMail($soat, $client, $vehicle);
        // $a = \App\Client::with('soats')->find(2);
        // return "<a href='https://mail.google.com/mail/?view=cm&fs=1&to=someone@gmail.com&su=subject_here&body=body_here'>mail</a>";
    }

    /**
     * Envía un correo de alerta Soat a un cliente específico
     *
     * @param int $id número de soat
     * @return
     */
    public function singleMailTest(int $id)
    {
        $soat = Soat::find($id);
        $client = SoatsController::createClient($soat);
        $res = Mail::to(TEST_EMAIL_ADMIN)->send(new SoatsAlert($client));
        echo $res;
    }

    public function notifyExp(int $id)
    {
        if (!$soat = Soat::find($id)) return;

        $client = SoatsController::createClient($soat);

        if ($email = \App\Email::find($client->emailid)) {

            $to = preg_replace("/\/|\s/", "", $email->email); // para depurar
            Mail::to($to)->send(new SoatsAlert($client));
            $soat->status_id = 6; // notificado por correo
            $soat->save();
            return ['status' => true];
        }
        else {
            return ['status' => false];
        }

    }

    public function update ($id, Request $request)
    {
        $soat = Soat::find($id);
        $status = $request->get('status_id');
        $soat->status_id = $status;
        if ($status == 1) $soat->renewed = true;
        $soat->save();

        return $soat;
    }

    public function store(Request $request)
    {
        // var_dump($request->all()); die;
        $soat = new Soat;

        // $soat->number = $request->get('number', null);
        $soat->created = $request->get('created', null);
        $soat->from = $request->get('from', null);
        $soat->exp = $request->get('exp');
        $soat->amount = $request->get('amount', null);
        $soat->use = $request->get('use');
        $soat->client_id = $request->get('client_id');
        $soat->vehicle_id = $request->get('vehicle_id');
        $soat->insurer_id = $request->get('insurer_id');
        $soat->ct = $request->get('ct', null);
        // $soat->class_id = $request->get('class_id', null);


        $soat->save();

        Soat::renewed( $request->get('oldsoat_id') );

        return Soat::with('client')->find($soat->id);

    }

    /************** FUNCIONES DE RESPUESTA ***********************************/

    /**
    * Envía correos avisando el vencimiento a la lista
    *
    * @param array $list de stdClass
    * @return array los nombres de los que no se les pudo mandar correo
    */
    public static function batchMail($list)
    {
        $empties = [];

        foreach ($list as $client) {

            if ($email = \App\Email::find($client->email)) {

                $to = preg_replace("/\/|\s/", "", $email->email); // para depurar
                Mail::to($to)->send(new SoatsAlert($client));
            }
            else {
                $empties[] = $client->name;
            }
        }

        return $empties;
    }

    /**
     * Devuelve un array con los soats que se vencen en el periodo.
     *
     * @param int $months los meses de diferencia
     * @return array de stdClass
     */
    public static function checkExp($months=1)
    {
        $response = [];
        $today = new \DateTime;
        // n meses
        $diff = new \DateInterval("P{$months}M");
        $todayf = $today->format('Y-m-d');
        $nextDate = $today->add($diff)->format('Y-m-d');

        $list = Soat::where('exp', '>', $todayf)
            ->where('exp', '<', $nextDate)->get();

        if (empty($list->all())) return response()->json(null, 203);

        foreach ($list as $soat) {

            $response[] = SoatsController::createClient($soat);
        }
        return $response;
    }

    public function dlcheckExp()
    {
        $today = new \DateTime;
        $tmw = $today->modify('+1 day');

        $todayf = $today->format('Y-m-d');
        $tmrf = $tmr->format('Y-m-d');

        $list = Soat::where('exp', '>=', $todayf)
            ->where('exp', '<=', $tmwf)->get();

        foreach ($list as $soat) {
            Mail::to(TEST_EMAIL_ADMIN)->send(new DlSoatAlertMail($content));
        }
    }

    /********************* FUNCIONES LIMPIAS ********************************/

    /**
     * Devuelve el valor de renovación del soat
     *
     * @param App\Vehicle $vehicle
     * @param Illuminate\Database\Eloquent\Builder $collection
     * @return float | null
     */
    public static function getSoatTotal (Vehicle $vehicle, Builder $collection)
    {
        foreach ($collection->get() as $row) {
            // code...

            $res = SoatsController::check($vehicle, $row);

            if ($res == True) {
                return $total = SoatRate::where('crate', $row->crate)
                    ->first()->total;
            }
        }
        return null;
    }

    /**
     * Chequea si el vehículo cumple una condición del soat
     *
     * @param App\Vehicle $vehicle
     * @param App\SoatDesc $data el cod rate del soat
     * @return bool
     */
    public static function check (Vehicle $vehicle, SoatDesc $data)
    {
        return $vehicle->{$data->unit} >= $data->min && $vehicle->{$data->unit} <= $data->max;
    }

    /**
     * Crea el cliente para mandar el correo
     */
    public static function createClient(Soat $soat)
    {

        $client = Client::find($soat->client_id);
        if ($client->type == 'natural') {
            $person = \App\Person::find($client->clientable_id);
            $type = 'people';
        } else {
            $company = \App\Company::find($client->clientable_id);
            $type = 'companies';
        }

        $vehicle = Vehicle::find($soat->vehicle_id);
        $l = SoatDesc::where('ct', $soat->ct);

        $name = preg_replace("/(^\w*)\s.*/", "$1", $client->name);
        $name = ucfirst(strtolower($name));
        $phone = Phone::find($client->phone_id);

        $sms = Phone::where('client_id', $soat->client_id)->where('sms', true)->first();

        $total = (float)SoatsController::getSoatTotal($vehicle, $l);

        $content = new \stdClass;
        $content->type = $type;
        $content->soatid = $soat->id;
        $content->clientid = $client->id;
        // $content->name = $name;
        $content->fullname = $client->name;
        $content->patent = $vehicle->patent;
        $content->exp = preg_replace("/(\d{4})-(\d{1,2})-(\d{1,2})/", "$3/$2/$1", $soat->exp);
        $content->brand = $vehicle->brand;
        $content->total = $total > 0? number_format($total, 0, ',', '.') : '(consultar)' ;
        $content->emailid = $client->email_id;
        $content->email =$client->email? $client->email->email : null;
        $content->phone = $phone? $phone->number : null;
        $content->sms = $sms;
        // $content->status = $soat->status? $soat->status->name: null;
        if ($type == 'people') {
            $content->gender = $person->gender;
            $content->name = ucfirst(strtolower($person->name1));
            $content->lastname = ucfirst(strtolower($person->lastname1));
        } else {
            $content->name = $company->name;
        }
        $content->status = $soat->status;

        return $content;
    }
}
