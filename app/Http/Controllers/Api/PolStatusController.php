<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PolStatus;

class PolStatusController extends Controller
{
    /**
     * Devuelve los valores de pol_status
     *
     * @return json
     */
    public function list()
    {
        return PolStatus::all();
    }
}
