<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Client;
use App\Phone;
use App\Email;
use App\Person;
use App\Company;
use App\Address;
use App\Soat;

class ClientsController extends Controller
{
    /**
     * Devuelve los datos de un cliente
     *
     * @param int $id id del cliente
     * @return json
     */
    public function bkshow($id)
    {

        $client = Client::find($id);

        // si no existe retorna 203
        if (!$client) return response()->json(null, 203);

        $type = $client->ref;
        if ($type == 'people') {
            $extended = Person::find($client->ref_id);
        } else {
            $extended = Company::find($client->ref_id);
        }
        $phones = Phone::where('client_id', $id)->get();
        $emails = Email::where('client_id', $id)->get();
        $addresses = Address::where('client_id', $id)->get();
        $products = [];

        $products['soat'] = Soat::with('client', 'vehicle')->where('client_id', $id)->get();

        return compact(
            'client',   'type',     'emails',
            'phones',   'extended', 'addresses',
            'products');
    }

    /**
     * Devuelve los datos de un cliente
     *
     * @param int $id id del cliente
     * @return json
     */
    public function show($id)
    {
      $props = [];

      if ($att = Input::get('with')) {
          $att = explode(',', $att);

          if (in_array('expanded', $att)) {
              $props = array_merge($props, [
                  'clientable',
                  'emails',
                  'phones',
                  'addresses',
              ]);
          }
          if (in_array('soats', $att)) {
              $props[] = 'soats';
          }
      }

        if (!$client = Client::with($props)->find($id)) {
            // si no existe retorna 203
            return response()->json(null, 203);
        } else {
            return $client;
        }
    }

    /**
     * Devuelve todos los clientes
     *
     * @return json paginado
     */
    public function list()
    {
        if (Input::get('page')) {
            return Client::paginate(10);
        } else {
            return Client::all();
        }
    }
}
