<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    public function handle ($request, $next, $role)
    {
        if (!$this->auth()->check()) {
            die('hola papa');
            return redirect()->to('login')
                ->with('status', 'success')
                ->with('message', 'Por favor autenticarse');
        }

        if ($role == 'all') {
            return $next($request);
        }

        if ($this->auth()->guest() || $this->auth()->user()->hasRole($role)) {
            abort(403);
        }

        return $next($request);
    }
}
