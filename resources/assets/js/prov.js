/**
 * Se llama cuando se carga un archivo
 */
window.onChange = function (event) {
    var file = event.target.files[0];
    console.log(file);  // chequeo

    var reader = new FileReader();
    reader.onload = function (event) {
        console.log(event.target.result); // chequeo
        arr = newCSVToArray(event.target.result);
        console.log(arr);   // chequeo
        var data = analizeData(arr)
        console.log(data); // chequeo
        expandDataPersonsForm(data);
        expandDataCompaniesForm(data);

    };

    text = reader.readAsText(file);
}

/**
 * devuelve un array con encabezado de un string
 */
function newCSVToArray (text) {
    var response = [];
    var lines = text.split('\n');   // separar cada línea
    var isCab = true;           // marcador para la cabecera
    var head = [];              // la cabecera
    var data = [];                  // donde van datos extraídos

    // extraer cabecera y data
    lines.forEach(function (line, index) {
        if (isCab) {
            head = line.split(',');
            isCab = false;
        } else {
            // pasada la cabecera arma el array
            data[index - 1] = line.split(',');   // reajustar por falta de cab
        }
    });

    // reorganizar data en response
    data.forEach(function (ele, ind) {

        var temp = [];
        ele.forEach(function (val, pos) {
            // cuadra cada valor con su etiqueta
            temp[ head[pos] ] = val;
        });

        response[ind] = temp;
    });

    return response;
}

/**
 * Separa un string en apellidos y nombres
 */
function splitLongNames (longname) {
    var na1 = '';
    var na2 = '';
    var last1 = '';
    var last2 = '';
    var parts = longname.replace(/ +(?= )/g,'').split(' ');

    if (parts.length == 4){
        //
        last1 = parts[0];
        last2 = parts[1];
        na1 = parts[2];
        na2 = parts[3];

    } else if (parts.length == 3){
        //
        last1 = parts[0];
        last2 = parts[1];
        na1 = parts[2];

    } else if (parts.length == 2){
        //
        last1 = parts[0];
        na1 = parts[1];

    } else if (parts.length == 1){
        //
        last1 = parts[0];
    } else if (parts.length == 5){
        last1 = parts[0];
        last2 = parts[1] +' ' +parts[2];
        na1 = parts[3];
        na2 = parts[4];

    } else {
        console.log('no parseada');
        console.log(parts);
    }

    return [last1, last2, na1, na2];
}

/**
 * Devuelve un array con los emails encontrados en un string
 */
function checkEmail (input) {
    var emails = [];

    if (input == '') {
        return [];
    }

    // porque hay emails mal formados
    try {
        var count = input.match(/@/g || []).length;

    } catch (e) {
        return [];
    }

    if (count > 1) {
        return input.split(/\s|,|\//).filter(function (ele) {
            return ele.indexOf('@') != -1;
        });
    } else {
        return [input];
    }
}

/**
 * Devuelve un array con los teléfonos encontrados en un string
 */
function checkPhoneNumbers (numbers) {
    var phones = [];

    if (numbers.length <= 10) {
        phones = [numbers];
    } else if (numbers.length > 10) {
        if (numbers.indexOf('/') != -1) {
            phones = numbers.split('/');
        } else if (numbers.indexOf(' ') != -1) {
            phones = numbers.split(' ');
        }
    }
    return phones;
}

/**
 * Revisa las fechas estén en el formato DD/MM/AAAA
 */
function checkDate (date) {

    // var response = '';

    // identificar: re [AAAA/MM/DD, AA/MM/DD, DD/MM, DD/MM/AA],
    // reordenar: fn
    var cases = [
        {
            // mm/dd/aaaa
            re: /^(0?\d|1[0,2])\/([0-3]?\d)\/(\d{4})$/,
            fn: function (a, b, c, d) {
                return c + '/' + b + '/' + d;
            }
        },
        {
            // aaaa/mm/dd
            re: /^(\d{4})\/(0?\d|1[0,2])\/([0-3]?\d)$/,
            fn: function (a, b, c, d) {
                return d + '/' + c + '/' + b;
            }
        },
        {
            // aa/mm/dd
            re: /^([0-1]\d{1})\/(0?\d|1[0,2])\/([0-3]?\d)$/,
            fn: function (a, b, c, d) {
                return d + '/' + c + '/20' + b;
            }
        },
        {
            // dd/mm
            re: /^(0?\d|1[0,2])\/([0-3]?\d)$/,
            fn: function (a, b, c, d) {
                return b + '/' + c + '/2018';
            }
        },
        {
            // dd/mm/aa
            re: /^([0-3]?\d)\/(0?\d|1[0,2])\/([0-1]\d{1})$/,
            fn: function (a, b, c, d) {
                return b + '/' + c + '/20' + d;
            }
        }
    ];

    cases.forEach(function (item) {
        if (date.match(item.re)) {
            date = date.replace(item.re, item.fn);
        }
    });

    return date;
}


/**
 * Analiza el array cargado
 */
function analizeData (arr) {
    console.log('en analizeData');
    var response = [];

    arr.forEach(function (row) {

        if (row['NOMBRE']) {
            response.push(analizeRow(row));
        }
    });

    return response;
}

function checkCod (string, cod) {

    var cods = [
        {
            re: /moto/i,
            ct: 1,
            id: 1
        },
        {
            re: /camp|camion/i,
            ct: 2,
            id: 2
        },
        {
            re: /carga|mix/i,
            ct: 3,
            id: 3
        },
        {
            re: /ofic/i,
            ct: 4,
            id: 4
        },
        {
            re: /espec/i,
            ct: 4,
            id: 5
        },
        {
            re: /ambu/i,
            ct: 4,
            id: 6
        },
        {
            re: /bomber/i,
            ct: 4,
            id: 7
        },
        {
            re: /dipl/i,
            ct: 4,
            id: 8
        },
        {
            re: /autom|famil/i,
            ct: 5,
            id: 9
        },
        {
            re: /vehi.*6/i,
            ct: 6,
            id: 10
        },
        {
            re: /aut.*neg/i,
            ct: 7,
            id: 11

        },
        {
            re: /taxi/i,
            ct: 7,
            id: 12
        },
        {
            re: /microb.*has/i,
            ct: 7,
            id: 13
        },
        {
            re: /^bus/i,
            ct: 8,
            id: 14
        },
        {
            re: /^buset/i,
            ct: 8,
            id: 15
        },
        {
            re: /microb.*mas/i,
            ct: 8,
            id: 16
        },
        {
            re: /serv.*pub/i,
            ct: 9,
            id: 17
        },
        {
            re: /intermun/i,
            ct: 9,
            id: 18
        },
        {
            re: /esc/i,
            ct: 9,
            id: 19
        },
        {
            re: /turis/i,
            ct: 9,
            id: 20
        }

    ];
    var response = null;

    cods.forEach(function (item) {
        if (string.match(item.re)) {
            return response = item[cod];
        }
    });
    return response;
}

function checkUse (string) {
    var response = 'particular';
    var uses = [
        {
            re: /part|per/i,
            use: 'particular'
        },
        {
            re: /publ/i,
            use: 'publico'
        }
    ];

    uses.forEach(function (item) {
        if (string.match(item.re)) {
            return response = item.use;
        }
    });

    return response;
}

/**
 * Analiza cada entrada (fila)
 */
function analizeRow (row) {
    var client = {};
    var vehicle = {};
    var misc = {};
    var type = row['CEDULA/NIT'] > 2000000000 ? 'juridico' : 'natural';

    if (type == 'natural') {
        var names = splitLongNames(row['NOMBRE']);
        client = {
            lastname1: names[0],
            lastname2: names[1],
            name1: names[2],
            name2: names[3],
            cc: parseInt(row['CEDULA/NIT']),
            gen: row['GEN'] || null,
            phones: checkPhoneNumbers(row['TELF-CELULAR']),
            emails: checkEmail(row['E-MAIL']),
            address: row['DIRECCION'],
            type
        };
    } else if (type == 'juridico') {
        client = {
            name: row['NOMBRE'],
            nit: parseInt(row['CEDULA/NIT']),
            phones: checkPhoneNumbers(row['TELF-CELULAR']),
            emails: checkEmail(row['E-MAIL']),
            address: row['DIRECCION'],
            type
        };
    }

    vehicle = {
        cil: parseInt(row['CILINDRAJE']),
        year: parseInt(row['AÑO']),
        patent: row['PLACA'],
        brand: row['MARCA/MODELO'],
        type: row['VEHICULO'],
        class: checkCod(row['VEHICULO'], 'id')
    };

    soat = {
        exp: checkDate(row['HASTA']),
        // exp: row['HASTA'],
        company: 'SEGUROESTADO',
        ct: checkCod(row['VEHICULO'], 'ct'),
        use: checkUse(row['VEHICULO'])
    };

    misc = {
        contacto: row['CONTACTO']
    };

    return {
        client,
        vehicle,
        soat,
        misc,
    };
}

/**
 * Muestra los datos en el formulario de manera que puedan ser editados antes
 * de enviarlos
 */
function expandDataPersonsForm (data) {
    // expande los datos en cada fila para cada entrada del array
    data.forEach(function (row, index) {

        // llena la tabla de personas naturales
        if (row.client.type == 'natural') {

            var box = {};       // Objeto para encapsular las variables indirectas

            // son los campos para la tabla de personas
            var clients = [
                'name1',
                'name2',
                'lastname1',
                'lastname2',
                'cc',
                'gen',
                'phones',
                'emails',
                'address'
            ];

            // los campos misc
            var misc = [
                'contacto'
            ];

            // los campos de Soat
            var soat = [
                'company',
                'exp',
                'ct',
                'use'
            ];

            // los campos de la parte de vehículos
            var vehicles = [
                'type',
                'cil',
                'patent',
                'brand',
                'year',
                'class'
            ];

            var tr = document.createElement('tr');      // agrupar los td

            //
            clients.forEach(function (item, ind) {

                box['td'+item] = document.createElement('td');
                // phones y emails tienen múltiples campos
                if (item == 'phones' || item == 'emails') {
                    // crea un input para cada uno y lo une al td
                    try {
                        row.client[item].forEach(function (phone, in2) {
                            var input = document.createElement('input');
                            input.setAttribute('name', index + '_' + item + '-' + in2);
                            input.setAttribute('value', phone);
                            box['td'+item].append(input);
                        });

                    }
                    catch (e) {
                        console.log(row.client);
                    }
                } else {
                    box[item] = document.createElement('input');
                    // el nombre del input
                    box[item].setAttribute('name', index + '_' + item);

                    // box[item].value = row.client[item];
                    // el valor del input
                    box[item].setAttribute('value', row.client[item]);
                    // finalmente se agrupan en el tr
                    box['td'+item].append(box[item]);

                }
                tr.append(box['td'+item]);      // tr listo para pegar al tbody
            });

            vehicles.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.vehicle[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            soat.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.soat[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            misc.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.misc[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            var ty = document.createElement('input');
            ty.setAttribute('hidden', 'true');
            ty.setAttribute('name', index + '_type');
            ty.setAttribute('value', row.client.type);
            tr.append(ty);

            // finalmente se agrega al tbody
            document.getElementById('tbody_persons').append(tr);
        }
    });
}

/**
 * Empresas. Muestra los datos en el formulario de manera que puedan ser editados antes
 * de enviarlos
 */
function expandDataCompaniesForm (data) {
    // expande los datos en cada fila para cada entrada del array
    data.forEach(function (row, index) {

        // llena la tabla de personas juridicas
        if (row.client.type == 'juridico') {

            var box = {};       // Objeto para encapsular las variables indirectas

            // son los campos para la tabla de personas
            var clients = [
                'name',
                'nit',
                'phones',
                'emails',
                'address'
            ];

            // los campos misc
            var misc = [
                'contacto'
            ];

            // los campos de Soat
            var soat = [
                'company',
                'exp',
                'ct',
                'use'
            ];

            // los campos de la parte de vehículos
            var vehicles = [
                'type',
                'cil',
                'patent',
                'brand',
                'year',
                'class'
            ];

            var tr = document.createElement('tr');      // agrupar los td

            //
            clients.forEach(function (item, ind) {

                box['td'+item] = document.createElement('td');
                // phones y emails tienen múltiples campos
                if (item == 'phones' || item == 'emails') {
                    // crea un input para cada uno y lo une al td
                    row.client[item].forEach(function (phone, in2) {
                        var input = document.createElement('input');
                        input.setAttribute('name', index + '_' + item + '-' + in2);
                        input.setAttribute('value', phone);
                        box['td'+item].append(input);
                    });
                } else {
                    box[item] = document.createElement('input');
                    // el nombre del input
                    box[item].setAttribute('name', index + '_' + item);

                    // box[item].value = row.client[item];
                    // el valor del input
                    box[item].setAttribute('value', row.client[item]);
                    // finalmente se agrupan en el tr
                    box['td'+item].append(box[item]);

                }
                tr.append(box['td'+item]);      // tr listo para pegar al tbody
            });

            vehicles.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.vehicle[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            soat.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.soat[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            misc.forEach(function (item, ind) {
                box['td'+item] = document.createElement('td');

                box[item] = document.createElement('input');
                // el nombre del input
                box[item].setAttribute('name', index + '_' + item);

                // el valor del input
                box[item].setAttribute('value', row.misc[item]);
                // finalmente se agrupan en el tr
                box['td'+item].append(box[item]);
                tr.append(box['td'+item]);
            });

            var ty = document.createElement('input');
            ty.setAttribute('hidden', 'true');
            ty.setAttribute('name', index + '_type');
            ty.setAttribute('value', row.client.type);
            tr.append(ty);

            // finalmente se agrega al tbody
            document.getElementById('tbody_co').append(tr);
        }
    });
}
