
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
// import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Dialog from './components/dialog'

window.Vue = require('vue');
Vue.component('Soats', require('./components/Soats'));
Vue.component('Clients', require('./components/Clients'));
// Vue.component('Client', require('./components/Clients/Client'));
Vue.component('modal', require('./components/modal'));
// Vue.component('Soat', require('./components/Soats/Soat'));
// Vue.component('Renew', require('./components/Soats/Renew'));
Vue.component('Dialog', Dialog)

/* formato de datos */
require('./filters');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('paginate', require('vuejs-paginate'));

Vue.use(Vuetify)

const app = new Vue({
    el: '#app'
});
