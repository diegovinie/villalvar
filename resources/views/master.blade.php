<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
        <title>Villalvar Seguros</title>
        <script type="text/javascript">
          window.APP_URL = '{{ env('APP_URL') }}';
        </script>

    </head>
    <body>
      <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
          @if (Auth::check())
              @include('menu')
          @endif
        <main class="mdl-layout__content">
          <div id="app" class="page-content">
            <!-- Your content goes here -->
            @yield('content')
          </div>
        </main>
      </div>

      <script src="/js/app.js" charset="utf-8"></script>

      <!-- jQuery library -->
      {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}

      <!-- Latest compiled JavaScript -->
      {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}
      <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
      @yield('scripts')
    </body>
</html>
