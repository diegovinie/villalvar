@extends('coming-soon')

@section('modal')
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="text-align:center;background-color:#ccc">
          <div class="modal-header">
            <h4 class="modal-title" >Gracias por preferirnos</h4>
          </div>
          <div class="modal-body">
            <p>En breve nos pondremos en contacto con usted.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" style="background-color:#002E66;color:#ccc" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>
    <script defer type="text/javascript">
        window.onload = function () {
            $('#myModal').modal();

        }
    </script>
@endsection
