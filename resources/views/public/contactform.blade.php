<link rel="stylesheet" href="/css/contact-form.css">
<form id="contact-form" style="padding-bottom: 5px;" onsubmit="event.preventDefault();console.log(event);">
  <div class="contact-form-loader">
  </div>
  <div class="row">
    <div class="grid_6">
      <label class="name">
        <input
          type="text"
          name="name"
          placeholder="Nombre"
          value=""
          title="*Este campo es obligatorio."
          required
          />
      </label>

      <label class="email">
        <input
          type="text"
          name="email"
          placeholder="E-mail"
          value=""
          />
      </label>
      <label class="phone">
        <input
          type="text"
          name="phone"
          placeholder="Teléfono"
          value=""
          />
      </label>
    </div>

    <div class="grid_6">
      <label class="message">
        <textarea
          name="message"
            placeholder="Mensaje"
            data-constraints='@@Required @@Length(min=20,max=999999)'
            >
        </textarea>
      </label>
      </div>
    </div>
    <div class="ta__right">
      <button
        type="submit"
        class="link-1"
        name="button"
        target="#modal1"
        style="border: 1px solid #fff; background-color: #016237; padding:7px; border-radius:5px;"
        >
        Enviar mensaje
      </button>
    </div>
  <div id="modal1" class="modal fade response-message">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          ¡Su mensaje ha sido enviado! Responderemos tan pronto como sea posible.
        </div>
      </div>
    </div>
  </div>
</form>
