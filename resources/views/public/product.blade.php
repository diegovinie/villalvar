@extends('public.master')

@section('content')
  <section id="content" class="inset__1">
    <div class="container">
      <div class="row">
        <div class="grid_12">
          <h2 class="wow flipInX" style="margin-top: -60px; border:0px solid red;">{{ $product->producto }}</h2>
        </div>
        <div class="gallery">
          <div class="grid_6 wow bounceInUp" data-wow-delay="0.1s">
            <img class="gall_item" src="/images/productos/{{ $product->imagen }}" alt="{{ $product->producto }}">
          </div>
          <div class="grid_6 wow bounceInUp" data-wow-delay="0.4s">
            <p class="white" style="text-align: justify; text-justify: inter-word;">
              {{ $product->intro }}<br><br>{!! $product->descripcion !!}
            </p>
          </div>
          <div class="clear">
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
