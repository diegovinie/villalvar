<!DOCTYPE html>
<html lang="es">
<head>
  <title>Villalvar Seguros</title>
  <meta charset="utf-8">
  <meta name = "format-detection" content = "telephone=no" />
  <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="/css/animate.css">
  <link rel="stylesheet" href="/css/grid.css">
  <link rel="stylesheet" href="/css/style.css">
</head>

<body id="home" class="page1">

<div class="main">
  <!--========================================================
                            HEADER
  =========================================================-->
  @include('public.header')
  <!--========================================================
                            CONTENT
  =========================================================-->
  <div style="height: 500px; padding-top: 10px;">
    @yield('content')
  </div>

  <!--========================================================
                            FOOTER
  =========================================================-->
 </div>

 @include('public.footer')

 <script src="/js/jquery.js"></script>
 <script src="/js/jquery-migrate-1.2.1.js"></script>
 <script src="/js/jquery.equalheights.js"></script>
 <script src="/js/script.js"></script>
</body>
</html>
