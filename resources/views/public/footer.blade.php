<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_3 wow fadeInUp" data-wow-delay="0.2s" style="border-top: 2px solid #fff;">
        <span>Villalvar Seguros &copy; <em id="copyright-year"></em></span>
      </div>
      <div class="grid_3 wow fadeInUp" data-wow-delay="0.6s" style="border-top: 2px solid #fff;">
        <span><a href="http://www.mapache.com.co">Powered by Mapache</a></span>
      </div>
      <div class="grid_3 wow fadeInUp" data-wow-delay="1s" style="border-top: 2px solid #fff;">
        <span><a href="tratamientoInfo.html">Tratamiento de datos</a></span>
      </div>
      <div class="grid_3 wow fadeInUp" data-wow-delay="1.4s" style="border-top: 2px solid #fff;">
        <span><a href="privacidad.html">Política de privacidad</a></span>
      </div>
    </div>
  </div>
</footer>
