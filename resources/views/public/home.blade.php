@extends('public.master')

@section('content')
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h1 class="grid_4logo logo wow bounceInDown">
          <a href="{{ route('home') }}">
            <img src="/images/logo_vas.png" alt="Villalvar">
          </a>
        </h1>
        <div class="clear"></div>
      </div>
      <div class="grid_4 wow bounceInLeft">
        <div class="banner banner__theme-1">
          <div class="banner_pad">
          <div class="count"> * </div>
          <div class="banner_title">SOAT</div>
          Adquiera aquí su Seguro Obligatorio de Accidentes de Tránsito

        </div> <a href="{{ route('product', 12) }}" class="banner_link">Ver más <span></span><span></span></a>
        </div>
      </div>
      <div class="clear"></div>
      <div class="grid_4 wow bounceInLeft">
        <div class="banner banner__theme-2">
          <div class="maxheight">
            <div class="banner_pad">
              <div class="count"> * </div>
              <div class="banner_title">
                SEGUROS TODO RIESGO
              </div>
              Proteja su vehículo en caso de accidente o hurto.
            </div>
          </div>
          <a href="{{ route('product', 17) }}" class="banner_link">Ver más<span></span><span></span></a>
        </div>
      </div>
      <div class="grid_4 wow bounceInUp">
        <div class="banner banner__theme-3">
          <div class="maxheight">
            <div class="banner_pad">
              <div class="count"> * </div>
              <div class="banner_title">SEGURO DE ARRIENDO</div>
              Despreocúpese por el pago de sus inquilinos.
            </div>
          </div>
          <a href="{{ route('product', 9) }}" class="banner_link">Ver más<span></span><span></span></a>
        </div>
      </div>
      <div class="grid_4 wow bounceInRight">
        <div class="banner banner__theme-4">
          <div class="maxheight">
            <div class="banner_pad">
              <div class="count"> * </div>
              <div class="banner_title">PÓLIZAS DE SALUD</div>
              Acceda a una mejor opción de servicios de salud.
            </div>
          </div>
          <a href="{{ route('product', 3) }}" class="banner_link">Ver más<span></span><span></span></a>
        </div>
      </div>
      <div class="grid_4 wow bounceInLeft">
        <div class="banner banner__theme-5">
          <div class="maxheight">
            <div class="banner_pad">
              <div class="count"> * </div>
              <div class="banner_title">PÓLIZAS DE CUMPLIMIENTO Y JUDICIALES</div>
              Cumpla con este requisito legal / contractual.
            </div>
          </div>
          <a href="{{ route('product', 19) }}" class="banner_link">Ver más<span></span><span></span></a>
        </div>
      </div>
        <div class="grid_8 wow bounceInRight">
          <div class="banner-2">
            <div class="banner-2_title">
            nuestros <span style=" text-transform: uppercase;"> asesores</span>
            expertos
            </div>
            <strong>Lo guiarán en la elección la mejor opción de acuerdo a sus necesidades .</strong>
            <div class="banner-2_phone">(57+1) 282 4265<br>(57+1) 565 9866</div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="grid_3 wow bounceInLeft" data-wow-delay="0.8s">
          <div class="block-1">
            <i class="fa fa-motorcycle" aria-hidden="true" style="font-size:64px;"></i><br><br>
          <!--<img src="images/icon1.png" alt="">-->
            <div class="text-1">
              Entrega a<br> domicilio*
            </div>
            Llevamos su póliza a su casa u oficina.<br><br>
            Indique  a su asesor el lugar donde debe ser entregada la póliza.<br>
            *<span style="font-size: 0.7em;">Aplican restricciones</span>
          </div>
        </div>
        <div class="grid_3 wow bounceInLeft" data-wow-delay="0.6s">
          <div class="block-1">
            <i class="fa fa-lock" aria-hidden="true" style="font-size:64px;"></i><br><br><!--<img src="images/icon2.png" alt="">-->
            <div class="text-1">
              Pagos seguros <br> garantizados
            </div>
            Realice el pago de sus pólizas desde la comodidad de su hogar u oficina.<br><br>
            Nuestra plataforma de pagos en línea es segura y confiable.
          </div>
        </div>
        <div class="grid_3 wow bounceInLeft" data-wow-delay="0.4s">
          <div class="block-1">
            <i class="fa fa-credit-card" aria-hidden="true" style="font-size:64px;"></i><br><br><!--<img src="images/icon3.png" alt="">-->
            <div class="text-1">
              Múltiples medios<br> de pago
            </div>
            Pague sus polizas con tarjeta de crédito o débito, en puntos efecty&copy; o en bancos autorizados.
          </div>
        </div>
        <div class="grid_3 wow bounceInLeft" data-wow-delay="0.2s">
          <div class="block-1">
            <i class="fa fa-repeat " aria-hidden="true" style="font-size:64px;"></i><br><br><!--<img src="images/icon4.png" alt="">-->
            <div class="text-1">
              <a href="">Recordamos  <br> por usted</a>
            </div>
            Estamos siempre pendientes de la fecha de renovación de sus pólizas, para que usted esté siempre protegido.
          </div>
        </div>
    </div>
  </div>
@endsection
