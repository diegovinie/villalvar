<header id="header">
  <span></span>
  <section id="stuck_container">
    <div class="container">
      <div class="row">
        <div class="grid_12">
          <div class="navigation">
            <nav>
              <ul class="sf-menu">
                <li><a href="{{ route('home') }}">Inicio</a></li>
                <li>
                  <a>Personales</a>
                  <ul>
                    @foreach ($menuItems['proPer'] as $product)
                      <li>
                        <a href="{{ route('product', $product->id) }}">{{ $product->producto }}</a>
                      </li>
                    @endforeach
                  </ul>
                </li>
                <li><a>Empresariales</a>
                  <ul>
                    @foreach ($menuItems['proEmp'] as $product)
                      <li>
                        <a href="{{ route('product', $product->id) }}">{{ $product->producto }}</a>
                      </li>
                    @endforeach
                  </ul>
                </li>
                <li><a>Vehiculares</a>
                  <ul>
                    @foreach ($menuItems['proVeh'] as $product)
                      <li>
                        <a href="{{ route('product', $product->id) }}">{{ $product->producto }}</a>
                      </li>
                    @endforeach
                  </ul>
                </li>
                <li>
                  <a href="{{ route('contact') }}">contacto</a>
                </li>
                <li>
                  <a href="{{ route('login') }}">Usuarios</a>
                </li>
                @if (Auth::user())
                    <li><a>{{ Auth::user()->name }}</a>
                        <ul>
                            <li>
                                <a href="/logout">Cerrar sesión</a>
                            </li>
                        </ul>
                    </li>
                @endif
              </ul>
            </nav>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!--==============================
            Stuck menu
=================================-->
{{-- @include('public.stuckMenu') --}}



</header>
