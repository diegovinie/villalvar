@extends('public.master')

@section('content')
  <section id="content" class="inset__1">
    <div class="container">
      <div class="row">
        <div class="grid_12">
          <h2 class=" wow flipInX" style="margin-top: -60px; border:0px solid red;">Nuestra ubicación</h2>
          <div class="map">
            <figure class="wow fadeInUp">
              <iframe src="https://www.google.com/maps/d/embed?mid=1ZnGrP2bdcS1xnYvnEFsQAfO9sfI" width="1170" height="450"></iframe>
              <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d24214.807650104907!2d-74.07162!3d4.6043!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1395650655094" style="border:0"></iframe>-->
            </figure>
            <div class="wow fadeInUp">
            Nuestras oficinas se encuentran en Bogotá D.C., Colombia.
              {{-- <div class="clear"></div> --}}
              <div class="map_block-1">
                <span class="fa fa-home"></span>
                <address class="address-1">
                  Calle 18 # 6-56 <br> Oficina 602
                </address>
              </div>
            <div class="map_block-1">
              <span class="fa fa-phone"></span>
              (57+1) 282 4265 <br> (57+1) 565 9866
            </div>
            <div class="map_block-1">
              <span class="fa  fa-envelope"></span>
              <a href="mailto:seguros@villalvar.com">seguros@villalvar.com</a>
            </div>
            </div>
          </div>
          <div class="clear"></div><br>
          <h2 class=" wow flipInX" style="margin-top: -60px; border:0px solid red;">Escríbanos</h2>
          @include('public.contactform')
        </div>
      </div>
    </div>
  </section>
@endsection
