@extends('master')

@section('content')
    @if (isset($success) )
        <div class="row">
            <div class="col-md-12">
                {{ $success }}
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mdl-grid">
                    <form>
                        <input
                            style="display:none;"
                            onchange="onChange(event)"
                            type="file"
                            id="soat" />
                        <label for="soat" class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
                          <i class="material-icons">attach_file</i>
                        </label>
                        {{-- <input
                        class="custom-file-input"
                        type="file"
                        id="soat"
                        onchange="onChange(event)"> --}}
                    </form>
                </div>
                <hr />
                <div class="mdl-grid">
                    <div  style="resize:both;">
                        <form class="" action="{{ route('storeSoats') }}" method="post" >
                    <input type="text" name="_token" value="{{ csrf_token() }}" hidden>
                    <button
                        type="submit"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"
                        name="button">
                        Enviar</button>
                    <h3>Clientes Naturales</h3>
                    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <thead>
                            <tr>
                                <th colspan="8">Cliente</th>
                                <th colspan="5">Vehículo</th>
                                <th colspan="4">SOAT</th>
                                <th>Misc</th>
                            </tr>
                            <tr>
                                <th>Primer Nombre</th>
                                <th>Segundo Nombre</th>
                                <th>Primer Apellido</th>
                                <th>Segundo Apellido</th>
                                <th>Cédula</th>
                                <th>Género</th>
                                <th>Teléfonos</th>
                                <th>Email</th>
                                <th>Dirección</th>
                                <th>Vehiculo</th>
                                <th>Cil</th>
                                <th>Placa</th>
                                <th>Marca/Modelo</th>
                                <th>Año</th>
                                <th>Aseguradora</th>
                                <th>Vencimiento</th>
                                <th>Cod Tipo</th>
                                <th>Desc</th>
                                <th>Uso</th>
                                <th>Contacto</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_persons">
                        </tbody>
                    </table>
                    <h3>Clientes Jurídicos</h3>
                    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <thead>
                            <tr>
                                <th colspan="5">Cliente</th>
                                <th colspan="5">Vehículo</th>
                                <th colspan="4">SOAT</th>
                                <th>Misc</th>
                            </tr>
                            <tr>
                                <th>Nombre</th>
                                <th>NIT</th>
                                <th>Teléfonos</th>
                                <th>Email</th>
                                <th>Dirección</th>
                                <th>Vehiculo</th>
                                <th>Cil</th>
                                <th>Placa</th>
                                <th>Marca/Modelo</th>
                                <th>Año</th>
                                <th>Aseguradora</th>
                                <th>Vencimiento</th>
                                <th>Cod Tipo</th>
                                <th>Desc</th>
                                <th>Uso</th>
                                <th>Contacto</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_co">

                        </tbody>
                    </table>
                    <button
                        type="submit"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"
                        name="button">Enviar</button>
                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/prov.js') }}" charset="utf-8"></script>
@endsection
