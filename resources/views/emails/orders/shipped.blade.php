@component('mail::message')
# Soats a vencer en los próximos {{ $months }} meses

@component('mail::table')
| Nombre             | Placa            | Expiración    | Teléfono                                                                            |
|:------------------ | ---------------- | ------------- | -----------------------------------------------------------------------------------:|
@foreach ($soats as $s)
| {{ $s->fullname }} | {{ $s->patent }} | {{ $s->exp }} | @component('mail::checkbox', ['type' => 'checkbox', 'name' => 'hola']) |
@endforeach
@endcomponent

@component('mail::button', ['color' => 'green', 'url' => 'http://google.co.ve'])
Procesar
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
