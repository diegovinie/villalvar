<header class="mdl-layout__header">
  <div class="mdl-layout__header-row">
    <!-- Title -->
    <span class="mdl-layout-title">Villalvar Seguros</span>
    <!-- Add spacer, to align navigation to the right -->
    <div class="mdl-layout-spacer"></div>
    <!-- Navigation. We hide it in small screens. -->
    <nav class="mdl-navigation mdl-layout--large-screen-only">
      <a class="mdl-navigation__link">Ayuda</a>
      <a class="mdl-navigation__link" href="{{ route('home') }}">Ir al Inicio</a>
      <a class="mdl-navigation__link" href="{{ route('logout') }}">Cerrar Sesión</a>
    </nav>
  </div>
</header>
<div class="mdl-layout__drawer">
  <span class="mdl-layout-title">Módulos</span>
  <nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="{{ route('clients') }}">Clientes</a>
    <a class="mdl-navigation__link" href="{{ route('soats') }}">Soats</a>
    <a class="mdl-navigation__link" href="{{ route('soatsBatch') }}">Agregar Lista de Soats</a>
    <a class="mdl-navigation__link">Vehículos</a>
    <a class="mdl-navigation__link" href="{{ route('checkExp', 1) }}">Vencimientos Próximos</a>
  </nav>
</div>
