@extends('master')

@section('content')
<div  class="mdl-grid">
    <div class="mdl-cell mdl-cell--6-col mdl-cell--3-offset">
        <div class="demo-card-wide mdl-shadow--2dp">
            <div class="mdl-card__title" style="background: url('/images/logo_vas.png') center / cover;height: 230px;width:600px; margin:10px;">
            </div>
            <div style="padding:20px;">
                <h2 class="mdl-card__title-text">Área de Usuarios</h2>

            </div>
            <div class="mdl-card__supporting-text">
                Puedes identificarte con alguna de siguientes opciones. Villalvar no almacena contraseñas de ningún tipo.
            </div>
            <div class="mdl-card__actions mdl-card--border">
                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/auth/google">
                    Ingresa con Google
                </a>
                <a target="_blank" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="https://accounts.google.com/logout?redirect_uri=http://localhost:8000/auth/google">
                    Cambiar de cuenta Google
                </a>
            </div>
        </div>
    </div>
</div>


{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <a class="btn btn-primary" href="/auth/google">
                        Log In With Google
                    </a>
                    <a class="btn btn-default" href="https://accounts.google.com/logout?redirect_uri=http://localhost:8000/auth/google">
                        Cambiar de cuenta Google
                    </a>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
