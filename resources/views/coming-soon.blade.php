<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="villalvar,seguros,soat,vida,todo riesgo">
    <meta name="author" content="">

    <title>Villalvar Seguros</title>

    <!-- Bootstrap core CSS -->
    <link href="startbootstrap-coming-soon/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="startbootstrap-coming-soon/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="startbootstrap-coming-soon/css/coming-soon.min.css" rel="stylesheet">


  </head>

  <body>

    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
      <source src="startbootstrap-coming-soon/mp4/bg.mp4" type="video/mp4">
    </video>

    <div class="masthead">
      <div class="masthead-bg"></div>
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto">
            <div class="masthead-content text-white py-5 py-md-0">
              <h1 class="mb-3">¡Próximamente!</h1>
              <p class="mb-5">Trabajamos duro para terminar el desarrollo de este sitio. Nuestra fecha de estreno es en
                <strong>agosto de 2018</strong>! Registra tu email para ponerte en contacto con nosotros</p>
              {{-- <div class="input-group input-group-newsletter">
                <input type="email" class="form-control" placeholder="Escribe tu email..." aria-label="Enter email..." aria-describedby="basic-addon">
                <div class="input-group-append">
                  <button class="btn btn-secondary" type="button">Contáctame!</button>
                </div>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="social-icons">
        <img style="position:absolute;right:0px;top:-350px;" src="{{ url('images/villalvar_logo_tr.png') }}" alt="">
      <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
          <a href="{{ url('/home')}}">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="{{ route('soatsBatch') }}">
            <i class="fa fa-facebook"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="#">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
      </ul>
    </div>
    @yield('modal')

    <!-- Bootstrap core JavaScript -->
    <script src="startbootstrap-coming-soon/vendor/jquery/jquery.min.js"></script>
    <script src="startbootstrap-coming-soon/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="startbootstrap-coming-soon/js/coming-soon.min.js"></script>

  </body>

</html>
