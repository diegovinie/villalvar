<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="">
            <div class="row">
                Se presentan los a vencer en los próximos {{ $months }} meses:
            </div>
            <form class="" action="soats/form" method="post">
                <table>
                    <tr>
                        <th>Cliente</th>
                        <th>Teléfono</th>
                        <th>Placa</th>
                        <th>Vencimiento</th>
                        <th>Valor de renovación</th>
                        <th>Enviar correo</th>
                        <th>Estatus</th>
                    </tr>
                    @foreach ($soats as $key => $soat)
                        <tr style="text-align:left;">
                            <td>{{ $soat->fullname }}</td>
                            <td style="text-align:right;">{{ $soat->phone }}</td>
                            <td style="text-align:center;">{{ $soat->patent }}</td>
                            <td style="text-align:right;">{{ $soat->exp }}</td>
                            <td style="text-align:right;">{{ $soat->total }}</td>
                            <td style="text-align:center;">
                                @if ($soat->email)
                                    <a href="https://mail.google.com/mail/?view=cm&fs=1&to={{ $soat->email }}&su={{ rawurlencode('Vencimiento de Soat') }}&body=" target="_blank">Redactar</a>
                                @endif
                            </td>
                            <td style="text-align:center;">{{ $soat->status? $soat->status->name : 's/d' }}</td>
                        </tr>
                    @endforeach
                </table>
                @component('mail::button', ['url' => route('checkExp', $months)])
                Revisar en Sistema
                @endcomponent

                {{-- <button type="submit" name="submit">Procesar</button> --}}
                {{-- <a href="localhost:8000/soats/ver">proc</a> --}}
            </form>
        </div>
    </body>
</html>
