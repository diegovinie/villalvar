<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Aviso Soat</title>
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        @if ($client->type == 'people')
            Estimad{{ $client->gender == 'F'? 'a' : 'o' }} Sr{{ $client->gender == 'F'? 'a.' : '.' }} {{ $client->lastname }},
        @else
            Sres. {{ $client->name }},
        @endif

        <p>Me permito informarle{{ $client->type == 'companies'? 's' : '' }} que está próxima la fecha de vencimiento del Seguro Obligatorio (SOAT) </p>
        <p>del vehículo <b>{{ $client->brand }}</b> placas <b>{{ $client->patent }}</b>.</p>
        <ul>
            <li>Fecha de vencimiento: <b>{{ $client->exp }}</b></li>
            <li>Valor de renovación: $ <b>{{ $client->total }}</b></li>
        </ul>
        <p>Si desea{{ $client->type == 'companies'? 'n' : '' }} que procedamos con la renovación haciendo <a id="link" href="{{ route('renew', $client->soatid) }}">click aquí</a> o respondiendo este correo.</p>
        <p>Atentamente,</p>
        <a href="{{ route('home')}}"><img src="{{ URL::asset('images/villalvar_logo.png')}}" alt=""></a>
        <p><small>Calle 18 N° 6-56 Oficina 602 </small></p>
        <p><small>Tel 282 4265 / 565 9866</small></p>
    </body>
</html>
