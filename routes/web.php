<?php
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$admin = 'admin';
$users = 'usuarios';

/******************* Sitio ***************************************/

Route::get('/', 'Mch\HomeController@index')->name('home');
Route::get('producto/{id}', 'Mch\HomeController@product')->name('product');
Route::get('contacto', 'Mch\HomeController@contact')->name('contact');

Route::name('comingsoon')->get('coming-soon', function () {
    return view('coming-soon');
});

Route::name('thanks')->get('thanks', function () {
    return view('thanks');
});

/*********************** Autorización **********************************/
Route::name('social.redirect')->get('auth/{provider}', 'SocialController@redirect');
Route::name('social.handle')->get('auth/handle/{provider}', 'SocialController@handle');

/****************** Administradores **************************************/

Route::group(['prefix' => $admin, 'middleware' => 'auth'], function () {
    /**********************CLIENTS*****************************************/

    Route::get('clients', 'ClientsController@index')->name('clients');
    Route::get('clients/{id}', 'ClientsController@show')->name('client');

    /*para agregar datos csv*/
    Route::get('soats', 'SoatsController@list')->name('soats');
    Route::get('soats/batch', 'SoatsController@batch')->name('soatsBatch');
    Route::post('soats/batch', 'SoatsController@storeBatch')->name('storeSoats');
    Route::get('soats/check/{months}', 'SoatsController@checkExp')->name('checkExp');
    Route::get('soats/{id}', 'SoatsController@show')->name('singleSoat');
});

Route::group(['prefix' => $users], function () {
    Route::name('users')->get('/', function () {
        return 'Usuarios';
    });
});


/***********************SOATS***************************************/

/*muestra lista de soats que se vencen*/



// Formularion de renovación
Route::get('soats/{id}/renewform', 'SoatsController@renewform')
    ->name('soats.renewform');
// cliente informa que desea renovar
Route::get('soats/{soatid}/renew', 'Api\SoatsController@notifyRenew')
    ->name('soats.renew');

Auth::routes();

Route::get('logout', function () {
    $coo = \Cookie::forget('user');
    $user = Auth::user();
    if ($user) {
        $user->session_exp = null;
        $user->save();
        Auth::logout();
    }

    return redirect()->route('home')->withCookie($coo);
});

Route::get('test', function () {
  return view('test');
});
