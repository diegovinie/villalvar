<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Socialite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

function getApiTokenFromCookie ($request) {
    $token = $request->cookie('user');

    if (!$token) {
        $token = $request->header('vil_token');
    }

    if (!$token) {
        $token = $request->get('user');
    }
    return $token;
}

$apiClientsMiddleware = function ($request, $next) {

    $email = getApiTokenFromCookie($request);

    if (!$email) {
        $status = [
            'code' => 0,
            'message' =>'No está identificado.'
        ];
        return response()->json($status, 401);
    }

    $user = App\User::whereEmail($email)->first();

    if (!$user) {
        $status = [
            'code' => 1,
            'message' => 'No registrado.'
        ];
        return response()->json($status, 401);
    }

    $expire = new DateTime($user->session_exp);

    if ($expire > new DateTime) {
        Auth::login($user);
        return $next($request);

    } else {
        $status = [
            'code' => 2,
            'message' => 'Sesión expirada.'
        ];
        return response()->json($status, 401);
    }
};

$apiAdminMiddleware = function ($request, $next) {

    $email = getApiTokenFromCookie($request);

    if (!$email) {
        $status = [
            'code' => 0,
            'message' =>'No está identificado.'
        ];
        return response()->json($status, 401);
    }

    $user = App\User::whereEmail($email)->first();

    if (!$user) {
        $status = [
            'code' => 1,
            'message' => 'No registrado.'
        ];
        return response()->json($status, 401);
    }

    if ($user->role !== 'admin') {
        $status = [
            'code' => 3,
            'message' => 'No tiene permiso.'
        ];
        return response()->json($status, 403);
    }

    $expire = new DateTime($user->session_exp);

    if ($expire > new DateTime) {
        Auth::login($user);
        return $next($request);

    } else {
        $status = [
            'code' => 2,
            'message' => 'Sesión expirada.'
        ];
        return response()->json($status, 401);
    }
};

///////////////////////// API v1 ///////////////////////////////////////////////
Route::group(['prefix' => 'v1'], function () use ($apiClientsMiddleware, $apiAdminMiddleware){

    //////////////////// ADMINISTRADORES ////////////////////////////////////
    Route::middleware($apiAdminMiddleware)->group(function () {

        /*************CLIENTS*********************************/

        /*datos un cliente*/
        Route::get('clients/{id}', 'Api\ClientsController@show')->name('api.clients.show');
        Route::get('clients', 'Api\ClientsController@list')->name('api.clients.list');

        /********** SOATS ***********************************/

        /*actualiza un status*/
        Route::put('soats/{id}/update', 'Api\SoatsController@update');

        Route::get('soats/', 'Api\SoatsController@list')->name('api.soats.list');
        /*datos un soat*/
        Route::get('soats/{id}', 'Api\SoatsController@show')->name('api.soats.show');
        /*los soats que se vencen en los próximos $months*/
        Route::get('soats/check/{months?}', 'Api\SoatsController@checkExp')->name('api.soats.check');
    });

    //////////////////// USUARIOS ////////////////////////////////////
    Route::middleware($apiClientsMiddleware)->group(function () {

        Route::get('/user', function( Request $request ){
            return $request->user();
        });

        /************STATUS***************************************/

        /*lista los tipos de status de pólizas*/
        Route::get('polstatus', 'Api\PolStatusController@list');
    });
});

Route::get('gettoken', function (Request $request) {
    $user = Socialite::driver('google')->user();
    var_dump( $user->getEmail() ); die;
    // var_dump($request->header()); die;
});


Route::get('soats/check/{months}/alert', 'Api\SoatsController@alertEmail');
Route::post('soats/form', 'Api\SoatsController@show');
Route::post('soats/store', 'Api\SoatsController@store');

/*cliente notifica que quiere renovar*/
// Route::get('soats/{soatid}/renew', 'Api\SoatsController@notifyRenew')
//     ->name('renew');
/*correo a un cliente notificando que se vence*/
Route::get('soats/{id}/notify', 'Api\SoatsController@notifyExp')
    ->name('notifyExp');
/*Para probar cosas*/
Route::get('soats/test', 'Api\SoatsController@test');
/*revisar*/
Route::get('soats/test/correo/cliente/{id}', 'Api\SoatsController@singleMailTest');
/*revisar*/
Route::get('soats/test/{id}/showmail', 'Api\SoatsController@');
